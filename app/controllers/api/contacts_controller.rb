class Api::ContactsController < ApplicationController
  respond_to :json
  before_action :ensure_json_request

  def index
    respond_with Contact.all
  end

  def show
    contact = Contact.find_by(id: params[:id])
    if contact
      respond_with :api, contact # contact.to_json
    else
      respond_with(:api, {status: "404", message: "Could not find contact with id " + params[:id]})
    end
  end

  def create
    contact = Contact.create(contact_params)
    if contact.persisted?
      respond_with :api, contact  # need to specify the :api namespace here
    else
      respond_with(:api, {status: "422", message: "Invalid contact parameters.", errors: contact.errors.to_json})
    end
  end

  def update
    contact = Contact.find(params[:id])
    if contact.update_attributes(contact_params)
      respond_with :api, contact
    else
      respond_with({status: "401", message: "Could not update contact #{contact.errors.full_messages}"})
    end
  end

  private
  def contact_params
    # {contact: {name: "something", email: "something"}}
    params.require(:contact).permit(:name, :email)
  end

  def ensure_json_request
    unless request.format.json?
      render plain: "API only works with JSON. Your request was registered as #{request.format}", status: "400"
      return
    end
  end
end
